﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ASOmobile
{
    [Activity(Label = "Новости", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class ActivityNews : Activity
    {
        public static ListView newsListView;
        private List<string> newsItem;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_news);
            //var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.app_bar);
            //SetSupportActionBar(toolbar);

            newsListView = FindViewById<ListView>(Resource.Id.listViewNews);
            newsItem = new List<string>();
            newsItem.Add("Доступно новое обновление. Вы можете скачать его самостоятельно или обновить в личном кабинете приложения");
            newsItem.Add("Доступно новое обновление. Вы можете скачать его самостоятельно или обновить в личном кабинете приложения");
            newsItem.Add("Доступно новое обновление. Вы можете скачать его самостоятельно или обновить в личном кабинете приложения");

            ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, newsItem);
            newsListView.Adapter = adapter;
            //Второй вариант
            //newsListView.Adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, newsItem);
        }
    }
}