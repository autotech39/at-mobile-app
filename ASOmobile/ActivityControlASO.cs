﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Bluetooth;
using Android.Util;

namespace ASOmobile
{
    [Activity(Label = "ActivityControlASO", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class ActivityControlASO : AppCompatActivity
    {
        static TabLayout tabLayout;
        public static string TAG = "bluetooth";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            tabLayout = FindViewById<TabLayout>(Resource.Id.sliding_tabsIcon);
            FnInitTabLayout();
        }

        void FnInitTabLayout()
        {
            tabLayout.SetTabTextColors(Android.Graphics.Color.Aqua, Android.Graphics.Color.AntiqueWhite);
            //Fragment array
            var fragments = new Android.Support.V4.App.Fragment[]
            {
                new FragmentSensors(),
                new FragmentMessages(),
                new FragmentJournal(),
            };
            //Tab title array
            var titles = CharSequence.ArrayFromStringArray(new[] {
                GetString(Resource.String.strHome),
                GetString(Resource.String.strASO),
                GetString(Resource.String.strCloud),
                
            });

            var viewPager = FindViewById<ViewPager>(Resource.Id.viewpagerIcon);
            //viewpager holding fragment array and tab title text
            viewPager.Adapter = new TabsFragmentPagerAdapter(SupportFragmentManager, fragments, titles);
            // Give the TabLayout the ViewPager 
            tabLayout.SetupWithViewPager(viewPager);
            FnSetIcons();
        }

        void FnSetIcons()
        {
            tabLayout.GetTabAt(0).SetIcon(Resource.Drawable.sensorsLogo);
            tabLayout.GetTabAt(1).SetIcon(Resource.Drawable.messageLogo);
            tabLayout.GetTabAt(2).SetIcon(Resource.Drawable.journalLogo);
        }

        private void FnSetupTabIconsWithText()
        {
            View view = LayoutInflater.Inflate(Resource.Layout.custom_text, null);
            var custTabOne = view.FindViewById<TextView>(Resource.Id.txtTabText);
            custTabOne.Text = GetString(Resource.String.strHome);
            custTabOne.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.homeLogo, 0, 0, 0);
            tabLayout.GetTabAt(0).SetCustomView(custTabOne);

            View view1 = LayoutInflater.Inflate(Resource.Layout.custom_text, null);
            TextView custTabTwo = view1.FindViewById<TextView>(Resource.Id.txtTabText);//(TextView)LayoutInflater.Inflate (Resource.Layout.custom_text, null); ;
            custTabTwo.Text = GetString(Resource.String.strASO);
            custTabTwo.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.blueetoothEnabledLogo, 0, 0, 0);
            tabLayout.GetTabAt(1).SetCustomView(custTabTwo);

            View view2 = LayoutInflater.Inflate(Resource.Layout.custom_text, null);
            TextView custTabThree = view2.FindViewById<TextView>(Resource.Id.txtTabText);//(TextView)LayoutInflater.Inflate (Resource.Layout.custom_text, null); ;
            custTabThree.Text = GetString(Resource.String.strCloud);
            custTabThree.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.cloudOnLogo, 0, 0, 0);
            tabLayout.GetTabAt(2).SetCustomView(custTabThree);

            View view3 = LayoutInflater.Inflate(Resource.Layout.custom_text, null);
            TextView custTabFour = view3.FindViewById<TextView>(Resource.Id.txtTabText);
            custTabFour.Text = GetString(Resource.String.strSettings);
            custTabFour.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.settingsLogo, 0, 0, 0);
            tabLayout.GetTabAt(3).SetCustomView(custTabFour);
        }

        /*public override void OnBackPressed()
        {
            

        }*/
        protected override void OnResume()
        {
            base.OnResume();
            try
            {
                /*FragmentSensors.txtYear.Text = MainActivity.txtYear;
                FragmentSensors.txtMonth.Text = MainActivity.txtMonth;
                FragmentSensors.txtDay.Text = MainActivity.txtMonth;
                FragmentSensors.txtData1.Text = MainActivity.txtData1;
                FragmentSensors.txtData2.Text = MainActivity.txtData2;
                FragmentSensors.txtData3.Text = MainActivity.txtData3;
                FragmentSensors.txtData4.Text = MainActivity.txtData4;
                FragmentSensors.txtData5.Text = MainActivity.txtData5;
                FragmentSensors.txtData6.Text = MainActivity.txtData6;
                FragmentSensors.txtData7.Text = MainActivity.txtData7;
                FragmentSensors.txtData8.Text = MainActivity.txtData8;
                FragmentSensors.txtData9.Text = MainActivity.txtData9;
                FragmentSensors.txtData10.Text = MainActivity.txtData10;*/
            }
            catch
            {

            }
            
        }
    }
}