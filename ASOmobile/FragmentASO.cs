﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Bluetooth;
using Java.Util;

namespace ASOmobile
{
    public class FragmentASO : Android.Support.V4.App.Fragment, View.IOnClickListener
    {
        static ListView listViewDevices;
        BluetoothAdapter bluetoothAdapter;
        Button btnConnect;
        Button btnDisconnect;
        Intent macAddress;
        BluetoothManager bluetoothManager = new BluetoothManager();
        public static string TAG = "bluetooth";
        public static ArrayAdapter<string> adapter;
        
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            //bluetoothManager = new BluetoothManager();
            View view = inflater.Inflate(Resource.Layout.fragment_aso, container, false);
            listViewDevices = view.FindViewById<ListView>(Resource.Id.listViewDevices);
            btnConnect = view.FindViewById<Button>(Resource.Id.buttonConnect);
            btnDisconnect = view.FindViewById<Button>(Resource.Id.buttonDisconnect);
            adapter = new ArrayAdapter<string>(Context, Android.Resource.Layout.SimpleListItemChecked);
            //adapter = new ArrayAdapter<string>(Context, Android.Resource.Layout.SimpleListItemChecked, devicesItem);
            listViewDevices.Adapter = adapter;
            bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
            var bondedDevices = bluetoothAdapter.BondedDevices;
            btnConnect.SetOnClickListener(this);
            btnDisconnect.SetOnClickListener(this);
            listViewDevices.ItemClick += ListViewDevices_ItemClick;
            foreach (var device in bondedDevices)
            {
                adapter.Add(device.Name + "\n" + device.Address);            
            }
            return view;   
        }

        private void ListViewDevices_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var info = (e.View as TextView).Text.ToString();
            var address = info.Substring(info.Length - 17);
            macAddress = new Intent();
            macAddress.PutExtra(bluetoothManager.MacAddress, address);
            //e.View.SetBackgroundColor(Android.Graphics.Color.LightGray);          
        }

        public void OnClick(View view)
        {           
            switch (view.Id)
            {
                /*case Resource.Id.listViewDevices:
                    var info = (view as TextView).Text.ToString();
                    var address = info.Substring(info.Length - 17);
                    macAddress = new Intent();
                    macAddress.PutExtra(bm.MacAddress, address);
                    bm.Connect(macAddress);
                    break;*/
                case Resource.Id.buttonConnect:
                    Toast.MakeText(Context, "Ждите, идет подключение к устройству", ToastLength.Short).Show();
                    bluetoothManager.Connect(macAddress);
                    break;
                case Resource.Id.buttonDisconnect:
                    bluetoothManager.Disconnect();
                    break;
            }
        }

        public ArrayAdapter<string> Adapter
        {
            get
            {
                return adapter;
            }
            set
            {
                adapter = value;
            }
        }
    }
}