﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Android.Bluetooth;

namespace ASOmobile
{
    

    class Receiver : BroadcastReceiver
    {
        private static ArrayAdapter<string> newDevicesArrayAdapter;
        public static readonly string GRID_STARTED = "GRID_STARTED";
        Activity _connectionOptions;

        public static string TAG = "bluetooth";

        public Receiver(Activity connectionOptions)
        {
            _connectionOptions = connectionOptions;
        }

        public override void OnReceive(Context context, Intent intent)
        {
            string action = intent.Action;

            if (action == BluetoothDevice.ActionAclDisconnected)
            {
                Log.Debug(TAG, "Связь разорвана");
                /*var newState = (State)intent.GetIntExtra(BluetoothAdapter.ExtraState, 0);
                Toast.MakeText(context, "Соединение потеряно", ToastLength.Short).Show();
                disconnectButton.Enabled = false;
                imageBluetoothStatus.SetImageResource(Resource.Drawable.imageBluetoothDisabled);
                textViewBattery.Text = "-----";
                MainActivity.textViewBattery.Text = "-----";
                context.StopService(new Intent(context, typeof(BluetoothReadService)));*/
            }

            if (action == BluetoothDevice.ActionAclDisconnectRequested)
            {
                Log.Debug(TAG, "Слабый сигнал");
                var newState = (State)intent.GetIntExtra(BluetoothAdapter.ExtraState, 0);
                Toast.MakeText(context, "Слабый сигнал", ToastLength.Short).Show();

            }

            if (action == BluetoothDevice.ActionAclConnected)
            {
                Log.Debug(TAG, "Соединение установлено");
                var newState = (State)intent.GetIntExtra(BluetoothAdapter.ExtraState, 10);
                Toast.MakeText(context, "Соединение установлено", ToastLength.Short).Show();
            }

            if (action == BluetoothAdapter.ActionStateChanged)
            {
                var newState = (State)intent.GetIntExtra(BluetoothAdapter.ExtraState, 0);
            }
            if (action == BluetoothAdapter.ActionScanModeChanged)
            {
                var newScanMode = (ScanMode)intent.GetIntExtra(BluetoothAdapter.ExtraScanMode, 0);
            }
            if (action == BluetoothDevice.ActionFound)
            {
                var newDevice = intent.GetParcelableExtra(BluetoothDevice.ExtraDevice) as BluetoothDevice;
                if (newDevice.BondState != Bond.Bonded)
                {
                    newDevicesArrayAdapter.Add(newDevice.Name + "\n" + newDevice.Address);
                }
            }
            else if (action == BluetoothAdapter.ActionDiscoveryFinished)
            {
                //_connectionOptions.SetProgressBarIndeterminateVisibility(false);
                _connectionOptions.SetTitle(Resource.String.selectDevice);
                if (newDevicesArrayAdapter.Count == 0)
                {
                    var noDevices = _connectionOptions.Resources.GetText(Resource.String.noneFound).ToString();
                    newDevicesArrayAdapter.Add(noDevices);
                }
            }
        }

    }
}