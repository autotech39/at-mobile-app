﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Bluetooth;
using Android.Util;

namespace ASOmobile
{
    [Activity(Label = "АСО", MainLauncher = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : AppCompatActivity
    {
        static TabLayout tabLayout;
        public static string TAG = "bluetooth";
        //FragmentASO frag = new FragmentASO();
        //private static ArrayAdapter<string> adapter;
        public static string txtTime;
        public static string txtSmoke;
        public static string txtBatASO;
        public static string txtBatAuto;
        public static string txtCurTemp;
        public static string txtFireTemp;
        public static string txtChangeTemp;
        public static string txtSpeed;
        public static string txtGPSLat;
        public static string txtGPSLong;
        public static string txtAccX;
        public static string txtAccY;
        public static string txtAccZ;

        public static string txtGyrX;
        public static string txtGyrY;
        public static string txtGyrZ;
        public static string txtMagX;
        public static string txtMagY;
        public static string txtMagZ;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);         
            SetContentView(Resource.Layout.Main);
            //RequestWindowFeature(WindowFeatures.NoTitle);
            //var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.app_bar);
            //SetSupportActionBar(toolbar);
            //SupportActionBar.SetDisplayShowTitleEnabled(false);
            //SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            tabLayout = FindViewById<TabLayout>(Resource.Id.sliding_tabsIcon);
            FnInitTabLayout();
            //frag.Adapter = adapter;

            Receiver receiver = new Receiver(this);
            var filter = new IntentFilter(BluetoothDevice.ActionFound);
            RegisterReceiver(receiver, filter);
            // Зарегистрируйтесь для трансляций, когда поиск закончен
            filter = new IntentFilter(BluetoothAdapter.ActionDiscoveryFinished);
            RegisterReceiver(receiver, filter);
            filter = new IntentFilter(BluetoothDevice.ActionAclDisconnected);
            RegisterReceiver(receiver, filter);
            filter = new IntentFilter(BluetoothDevice.ActionAclConnected);
            RegisterReceiver(receiver, filter);
            filter = new IntentFilter(BluetoothDevice.ActionAclDisconnectRequested);
            RegisterReceiver(receiver, filter);

            //DoDiscovery();
            //BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
            //bluetoothAdapter.StartDiscovery();
            checkBTState();
        }

        void FnInitTabLayout()
        {
            tabLayout.SetTabTextColors(Android.Graphics.Color.Aqua, Android.Graphics.Color.AntiqueWhite);
            //Fragment array
            var fragments = new Android.Support.V4.App.Fragment[]
            {
                new FragmentHome(),
                new FragmentASO(),
                new FragmentCloud(),
                new FragmentSettings(),
            };
            //Tab title array
            var titles = CharSequence.ArrayFromStringArray(new[] {
                GetString(Resource.String.strHome),
                GetString(Resource.String.strASO),
                GetString(Resource.String.strCloud),
                GetString(Resource.String.strSettings),
            });

            var viewPager = FindViewById<ViewPager>(Resource.Id.viewpagerIcon);

            //viewpager holding fragment array and tab title text
            viewPager.Adapter = new TabsFragmentPagerAdapter(SupportFragmentManager, fragments, titles);

            // Give the TabLayout the ViewPager 
            tabLayout.SetupWithViewPager(viewPager);
            FnSetIcons();
            //FnSetupTabIconsWithText();
        }

        void FnSetIcons()
        {
            tabLayout.GetTabAt(0).SetIcon(Resource.Drawable.homeLogo);
            tabLayout.GetTabAt(1).SetIcon(Resource.Drawable.bluetoothDisabledLogo);
            tabLayout.GetTabAt(2).SetIcon(Resource.Drawable.cloudOffLogo);
            tabLayout.GetTabAt(3).SetIcon(Resource.Drawable.settingsLogo);

        }

        private void FnSetupTabIconsWithText()
        {
            View view = LayoutInflater.Inflate(Resource.Layout.custom_text, null);
            var custTabOne = view.FindViewById<TextView>(Resource.Id.txtTabText);
            custTabOne.Text = GetString(Resource.String.strHome);
            custTabOne.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.homeLogo, 0, 0, 0);
            tabLayout.GetTabAt(0).SetCustomView(custTabOne);

            View view1 = LayoutInflater.Inflate(Resource.Layout.custom_text, null);
            TextView custTabTwo = view1.FindViewById<TextView>(Resource.Id.txtTabText);//(TextView)LayoutInflater.Inflate (Resource.Layout.custom_text, null); ;
            custTabTwo.Text = GetString(Resource.String.strASO);
            custTabTwo.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.blueetoothEnabledLogo, 0, 0, 0);
            tabLayout.GetTabAt(1).SetCustomView(custTabTwo);

            View view2 = LayoutInflater.Inflate(Resource.Layout.custom_text, null);
            TextView custTabThree = view2.FindViewById<TextView>(Resource.Id.txtTabText);//(TextView)LayoutInflater.Inflate (Resource.Layout.custom_text, null); ;
            custTabThree.Text = GetString(Resource.String.strCloud);
            custTabThree.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.cloudOnLogo, 0, 0, 0);
            tabLayout.GetTabAt(2).SetCustomView(custTabThree);

            View view3 = LayoutInflater.Inflate(Resource.Layout.custom_text, null);
            TextView custTabFour = view3.FindViewById<TextView>(Resource.Id.txtTabText);
            custTabFour.Text = GetString(Resource.String.strSettings);
            custTabFour.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.settingsLogo, 0, 0, 0);
            tabLayout.GetTabAt(3).SetCustomView(custTabFour);
        }

        public class Receiver : BroadcastReceiver
        {    
            Activity _connectionOptions;
            
            public Receiver(Activity connectionOptions)
            {
                _connectionOptions = connectionOptions;
            }

            public override void OnReceive(Context context, Intent intent)
            {
                string action = intent.Action;
                if (action == BluetoothDevice.ActionAclDisconnected)
                {
                    Log.Debug(TAG, "Связь разорвана");
                    var newState = (State)intent.GetIntExtra(BluetoothAdapter.ExtraState, 0);
                    Toast.MakeText(context, "Соединение потеряно", ToastLength.Short).Show();
                    tabLayout.GetTabAt(1).SetIcon(Resource.Drawable.bluetoothDisabledLogo);
                    context.StopService(new Intent(context, typeof(BluetoothReadService)));
                }

                if (action == BluetoothDevice.ActionAclDisconnectRequested)
                {
                    Log.Debug(TAG, "Слабый сигнал");                  
                    var newState = (State)intent.GetIntExtra(BluetoothAdapter.ExtraState, 0);
                    Toast.MakeText(context, "Слабый сигнал", ToastLength.Short).Show();
                }

                if (action == BluetoothDevice.ActionAclConnected)
                {
                    Log.Debug(TAG, "Соединение установлено");
                    tabLayout.GetTabAt(1).SetIcon(Resource.Drawable.blueetoothEnabledLogo);
                    var newState = (State)intent.GetIntExtra(BluetoothAdapter.ExtraState, 10);
                    Toast.MakeText(context, "Соединение установлено", ToastLength.Short).Show();
                    context.StartService(new Intent(context, typeof(BluetoothReadService)));
                }

                if (action == BluetoothAdapter.ActionStateChanged)
                {
                    var newState = (State)intent.GetIntExtra(BluetoothAdapter.ExtraState, 0);
                }
                if (action == BluetoothAdapter.ActionScanModeChanged)
                {
                    var newScanMode = (ScanMode)intent.GetIntExtra(BluetoothAdapter.ExtraScanMode, 0);
                }
                if (action == BluetoothDevice.ActionFound)
                {
                    var newDevice = intent.GetParcelableExtra(BluetoothDevice.ExtraDevice) as BluetoothDevice;
                    if (newDevice.BondState != Bond.Bonded)
                    {
                        FragmentASO.adapter.Add(newDevice.Name + "\n" + newDevice.Address);
                    }

                    /*foreach (var device in bondedDevices)
                    {
                        adapter.Add(device.Name + "\n" + device.Address);
                    }*/
                }
                else if (action == BluetoothAdapter.ActionDiscoveryFinished)
                {
                    //_connectionOptions.SetProgressBarIndeterminateVisibility(false);
                    _connectionOptions.SetTitle(Resource.String.selectDevice);
                    if (FragmentASO.adapter.Count == 0)
                    {
                        var noDevices = _connectionOptions.Resources.GetText(Resource.String.noneFound).ToString();
                        FragmentASO.adapter.Add(noDevices);
                    }
                }
            }
        }

        private void DoDiscovery()
        {
            BluetoothAdapter btAdapter = BluetoothAdapter.DefaultAdapter;
            /*if (Debug)
                Log.Debug(TAG, "doDiscovery()");*/

            // Indicate scanning in the title
            //SetProgressBarIndeterminateVisibility(true);
            //SetTitle(Resource.String.scanning);

            // Turn on sub-title for new devices
            //FindViewById<View>(Resource.Id.title_new_devices).Visibility = ViewStates.Visible;

            // If we're already discovering, stop it
            if (btAdapter.IsDiscovering)
            {
                btAdapter.CancelDiscovery();
            }

            // Request discover from BluetoothAdapter
            btAdapter.StartDiscovery();
        }

        private void checkBTState()
        {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
            const int REQUEST_ENABLE_BT = 1;
            if (bluetoothAdapter == null)
            {
                errorExit(TAG, "Fatal Error", "Bluetooth не поддерживается");
            }
            else
            {
                if (bluetoothAdapter.IsEnabled)
                {
                    Log.Debug(TAG, "...Bluetooth включен...");
                }
                else
                {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ActionRequestEnable);
                    StartActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                }
            }
        }

        private void errorExit(string TAG, string title, string message)
        {
            Toast.MakeText(BaseContext, title + " - " + message, ToastLength.Long).Show();
            Finish();
        }
        public override void OnBackPressed()
        {
            //base.OnBackPressed();
            /*if (BluetoothConnector.programWorking == true)
            {
                OpenQuitDialog();
            }
            if (BluetoothConnector.programWorking == false)
            {
                base.OnBackPressed();
            }*/
            //OpenQuitDialog();
            Toast.MakeText(this, "Для закрытия программы нажмите кнопку Выход", ToastLength.Short).Show();
        }

        private void OpenQuitDialog()
        {

            RunOnUiThread(() =>
            {
                Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                alert.SetTitle("Вы уверены, что хотите выйти?");
                //alert.SetMessage("Произвольная программа будет остановлена");
                /*alert.SetPositiveButton("Да", (senderAlert, args) =>
                {
                    base.OnBackPressed();
                    Finish();
                });
                alert.SetNegativeButton("Нет", (senderAlert, args) =>
                {

                });*/
                alert.SetPositiveButton("Да", (senderAlert, args) =>
                {
                    base.OnBackPressed();
                    Finish();
                });
                
                
                Dialog dialog = alert.Create();
                dialog.Show();
            });   
        }

        public void FinishProgram()
        {
            Finish();
        }
    }
}

