﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Bluetooth;
using Java.Util;
using System.Threading;
using System.IO;

namespace ASOmobile
{
    class BluetoothManager
    {
        //static Handler handler;
        public static byte[] rec = new byte[65];
        static BluetoothAdapter bluetoothAdapter;
        static BluetoothDevice bluetoothDevice;
        static BluetoothSocket bluetoothSocket;
        string macAddress;
        UUID MY_UUID = UUID.FromString("00001101-0000-1000-8000-00805F9B34FB");

        

        public string MacAddress
        {
            get
            {
                return macAddress;
            }
            set
            {
              macAddress = value;
            }
        }

        public void Connect(Intent data)
        {
            bluetoothAdapter = BluetoothAdapter.DefaultAdapter;
            try
            {
                bluetoothDevice = bluetoothAdapter.GetRemoteDevice(data.Extras.GetString(macAddress));
            }
            catch
            {

            }
            try
            {
                bluetoothSocket = bluetoothDevice.CreateRfcommSocketToServiceRecord(MY_UUID);
            }
            catch
            {

            }
            try
            {
                bluetoothSocket.ConnectAsync();
            }
            catch
            {

            }
            
        }

        public void Disconnect()
        {
            try
            {
                bluetoothSocket.Close();
            }
            catch
            {

            }
        }

        public static void Read(Context context)
        {
            var read = new Thread(() => {
                byte data;
                int cnt = 0;
                bool flag = true;
                byte crc_sum = 0;
                //double val;
                try
                {
                    while (true)
                    {
                        if (bluetoothSocket.InputStream.CanRead)
                        {
                            if (bluetoothSocket.InputStream.IsDataAvailable())
                            {
                                data = (byte)bluetoothSocket.InputStream.ReadByte();

                                if (flag == true && data == 170)
                                {
                                    flag = false;
                                    crc_sum = 0;
                                    cnt = 0;
                                }
                                rec[cnt] = data;                                
                                cnt++;
                                if (cnt > 64)
                                {
                                    if ((255 - crc_sum) == data)
                                    {
                                        

                                        FragmentSensors.handler.Post(() =>
                                        {
                                            //Send data  
                                            try
                                            {
                                                
                                                MainActivity.txtTime = $"{(rec[15] & 0x1F).ToString("0")}:{((rec[16] >> 2) & 0x3F).ToString("0")}" + "   " + $"{(((rec[18] >> 7) & 0x1) | ((rec[17] << 1) & 0x1E)).ToString("0")}.{"0" + (((rec[15] >> 5) & 0x7) | ((rec[14] << 3) & 0x8)).ToString("0")}.{"20" + ((rec[14] >> 1 & 0x7F)).ToString("0")}";
                                                //MainActivity.txtYear = "20"+((rec[14] >> 1 & 0x7F)).ToString("0");//0x7F год
                                                
                                                if ((rec[19] & 0x3) == 0)
                                                {
                                                    MainActivity.txtSmoke = "Дыма нет";
                                                }
                                                if ((rec[19] & 0x3) == 1)
                                                {
                                                    MainActivity.txtSmoke = "Дым есть";
                                                }

                                                MainActivity.txtBatASO = (rec[22] * 1.0 / 10).ToString("0.0") + "В";
                                                MainActivity.txtBatAuto = (rec[23] * 1.0 / 10).ToString("0.0") + "В";

                                                if(rec[24] > 200)
                                                {
                                                    MainActivity.txtCurTemp = (200 - rec[24]).ToString("0") + "С°";
                                                }
                                                else
                                                {
                                                    MainActivity.txtCurTemp = (rec[24]).ToString("0") + "С°";
                                                }

                                                if (rec[25] > 200)
                                                {
                                                    MainActivity.txtFireTemp = (200 - rec[25]).ToString("0") + "С°";
                                                }
                                                else
                                                {
                                                    MainActivity.txtFireTemp = (rec[25]).ToString("0") + "С°";
                                                }

                                                if (rec[26] > 127)
                                                {
                                                    MainActivity.txtChangeTemp = (127 - rec[26]).ToString("0") + "С°";
                                                }
                                                else
                                                {
                                                    MainActivity.txtChangeTemp = (rec[26]).ToString("0") + "С°";
                                                }

                                                MainActivity.txtSpeed = (rec[27]).ToString("0") + "км/ч";

                                                double mantLat = ((double)rec[28] * 256 * 256 * 256 * 256) + ((double)rec[29] * 256 * 256 * 256) + ((double)rec[30] * 256 * 256) + ((double)rec[31] * 256) + (double)rec[32];
                                                double mantLong = ((double)rec[33] * 256 * 256 * 256 * 256) + ((double)rec[34] * 256 * 256 * 256) + ((double)rec[35] * 256 * 256) + ((double)rec[36] * 256) + (double)rec[37];
                                                double mlat = ((rec[38] >> 4) & 0xF);
                                                double mlong = (rec[38] & 0xF);
                                                string dlat;
                                                string dlong;

                                                if ((rec[39] & 0x80) == 0)
                                                {
                                                    dlat = "N";
                                                }
                                                else
                                                {
                                                    dlat = "S";
                                                }

                                                if ((rec[39] & 0x40) == 0)
                                                {
                                                    dlong = "E";
                                                }
                                                else
                                                {
                                                    dlong = "W";
                                                }

                                                MainActivity.txtGPSLat = (mantLat / (Math.Pow(10, mlat))).ToString("0.00000000") + dlat;
                                                MainActivity.txtGPSLong = (mantLong / (Math.Pow(10, mlong))).ToString("0.00000000") + dlong;


                                                double accX = (double)rec[40] * 256 + rec[41];
                                                if(accX > 32767)
                                                {
                                                    accX = 32767 - accX;
                                                }

                                                double accY = (double)rec[42] * 256 + rec[43];
                                                if (accY > 32767)
                                                {
                                                    accY = 32767 - accY;
                                                }

                                                double accZ = (double)rec[44] * 256 + rec[45];
                                                if (accZ > 32767)
                                                {
                                                    accZ = 32767 - accZ;
                                                }

                                                double maccX = (rec[46] >> 4) & 0xF;
                                                double maccY = rec[46] & 0xF;
                                                double maccZ = (rec[47] >> 4) & 0xF;
 
                                                MainActivity.txtAccX = (accX / (Math.Pow(10, maccX))).ToString("0.00000") + "м/с²";
                                                MainActivity.txtAccY = (accY / (Math.Pow(10, maccY))).ToString("0.00000") + "м/с²";
                                                MainActivity.txtAccZ = (accZ / (Math.Pow(10, maccZ))).ToString("0.00000") + "м/с²";



                                                double gyrX = (double)rec[48] * 256 + rec[49];
                                                if (gyrX > 32767)
                                                {
                                                    gyrX = 32767 - gyrX;
                                                }

                                                double gyrY = (double)rec[50] * 256 + rec[51];
                                                if (gyrY > 32767)
                                                {
                                                    gyrY = 32767 - gyrY;
                                                }

                                                double gyrZ = (double)rec[52] * 256 + rec[53];
                                                if (gyrZ > 32767)
                                                {
                                                    gyrZ = 32767 - gyrZ;
                                                }

                                                double mgyrX = (rec[54] >> 4) & 0xF;
                                                double mgyrY = rec[54] & 0xF;
                                                double mgyrZ = (rec[55] >> 4) & 0xF;


                                                MainActivity.txtGyrX = (gyrX / (Math.Pow(10, mgyrX))).ToString("0.") + "рад";
                                                MainActivity.txtGyrY = (gyrY / (Math.Pow(10, mgyrY))).ToString("0") + "рад";
                                                MainActivity.txtGyrZ = (gyrZ / (Math.Pow(10, mgyrZ))).ToString("0") + "рад";


                                                double magX = (double)rec[56] * 256 + rec[57];
                                                if (magX > 32767)
                                                {
                                                    magX = 32767 - magX;
                                                }

                                                double magY = (double)rec[58] * 256 + rec[59];
                                                if (magY > 32767)
                                                {
                                                    magY = 32767 - magY;
                                                }

                                                double magZ = (double)rec[60] * 256 + rec[61];
                                                if (magZ > 32767)
                                                {
                                                    magZ = 32767 - magZ;
                                                }

                                                double mmagX = (rec[62] >> 4) & 0xF;
                                                double mmagY = rec[62] & 0xF;
                                                double mmagZ = (rec[63] >> 4) & 0xF;


                                                MainActivity.txtMagX = (magX / (Math.Pow(10, mmagX))).ToString("0.00000") + "нТл";
                                                MainActivity.txtMagY = (magY / (Math.Pow(10, mmagY))).ToString("0.00000") + "нТл";
                                                MainActivity.txtMagZ = (magZ / (Math.Pow(10, mmagZ))).ToString("0.00000") + "нТл";
                                            }
                                            catch
                                            {

                                            }

                                            try
                                            {
                                                //FragmentSensors.txtYear.Text = $"{(((rec[18] >> 7) & 0x1) | ((rec[17] << 1) & 0x1E)).ToString("0")}.{"0" + (((rec[15] >> 5) & 0x7) | ((rec[14] << 3) & 0x8)).ToString("0")}.{"20" + ((rec[14] >> 1 & 0x7F)).ToString("0")}";
                                                /*FragmentSensors.txtYear.Text = "20" + ((rec[14] >> 1 & 0x7F)).ToString("0");//0x7F год
                                                FragmentSensors.txtMonth.Text = (((rec[15] >> 5) & 0x7) | ((rec[14] << 3) & 0x8)).ToString("0");
                                                FragmentSensors.txtDay.Text = (((rec[18] >> 7) & 0x1) | ((rec[17] << 1) & 0x1E)).ToString("0");
                                                FragmentSensors.txtData1.Text = (rec[4]).ToString("0");
                                                FragmentSensors.txtData2.Text = (rec[5]).ToString("0");
                                                FragmentSensors.txtData3.Text = (rec[6]).ToString("0");
                                                FragmentSensors.txtData4.Text = (rec[7]).ToString("0");
                                                FragmentSensors.txtData5.Text = (rec[8]).ToString("0");
                                                FragmentSensors.txtData6.Text = (rec[9]).ToString("0");
                                                FragmentSensors.txtData7.Text = (rec[10]).ToString("0");
                                                FragmentSensors.txtData8.Text = (rec[11]).ToString("0");
                                                FragmentSensors.txtData9.Text = (rec[12]).ToString("0");
                                                FragmentSensors.txtData10.Text = (rec[0]).ToString("0");*/
                                            }
                                            catch
                                            {

                                            }

                                            try
                                            {
                                                //FragmentSensors.txtYear.Text = $"{(((rec[18] >> 7) & 0x1) | ((rec[17] << 1) & 0x1E)).ToString("0")}.{"0" + (((rec[15] >> 5) & 0x7) | ((rec[14] << 3) & 0x8)).ToString("0")}.{"20" + ((rec[14] >> 1 & 0x7F)).ToString("0")}";
                                                FragmentSensors.txtTime.Text = MainActivity.txtTime;//0x7F год
                                                FragmentSensors.txtSmoke.Text = MainActivity.txtSmoke;
                                                FragmentSensors.txtBatASO.Text = MainActivity.txtBatASO;
                                                FragmentSensors.txtBatAuto.Text = MainActivity.txtBatAuto;
                                                FragmentSensors.txtCurTemp.Text = MainActivity.txtCurTemp;
                                                FragmentSensors.txtFireTemp.Text = MainActivity.txtFireTemp;
                                                FragmentSensors.txtChangeTemp.Text = MainActivity.txtChangeTemp;
                                                FragmentSensors.txtSpeed.Text = MainActivity.txtSpeed;
                                                FragmentSensors.txtGPSLat.Text = MainActivity.txtGPSLat;
                                                FragmentSensors.txtGPSLong.Text = MainActivity.txtGPSLong;
                                                FragmentSensors.txtAccX.Text = MainActivity.txtAccX;
                                                FragmentSensors.txtAccY.Text = MainActivity.txtAccY;
                                                FragmentSensors.txtAccZ.Text = MainActivity.txtAccZ;
                                                FragmentSensors.txtGyrX.Text = MainActivity.txtGyrX;
                                                FragmentSensors.txtGyrY.Text = MainActivity.txtGyrY;
                                                FragmentSensors.txtGyrZ.Text = MainActivity.txtGyrZ;
                                                FragmentSensors.txtMagX.Text = MainActivity.txtMagX;
                                                FragmentSensors.txtMagY.Text = MainActivity.txtMagY;
                                                FragmentSensors.txtMagZ.Text = MainActivity.txtMagZ;
                                            }
                                            catch
                                            {

                                            }
                                        });
                                    }
                                    crc_sum = 0;
                                    cnt = 0;
                                    flag = true;
                                }
                                else
                                {
                                    crc_sum += data;
                                }
                            }

                        }
                    }
                }
                catch
                {

                }
            }
            );
            read.Start();
        }
    }
}