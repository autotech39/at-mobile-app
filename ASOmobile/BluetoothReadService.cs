﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace ASOmobile
{
    [Service]
    class BluetoothReadService : Service
    {        
        static readonly string TAG = "READ_SERVICE";
        public override void OnCreate()
        {
            base.OnCreate();
            try
            {
                //RegisterMyBroadcastReceiver(this);
                
                try
                {
                    BluetoothManager.Read(this);
                    Log.Debug(TAG, "Сервис запустился");
                }
                catch
                {
                    Log.Debug(TAG, "Сервис НЕ запустился");
                }
                
            }
            catch
            {

            }
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            //BluetoothConnector.Read(this);
            return StartCommandResult.Sticky;
        }

        public override IBinder OnBind(Intent intent)
        {
            // This is a started service, not a bound service, so we just return null.
            return null;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
        }
    }
}