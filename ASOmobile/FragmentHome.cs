﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace ASOmobile
{
    public class FragmentHome : Android.Support.V4.App.Fragment, View.IOnClickListener
    {
        Button btnNews;
        Button btnAccount;
        Button btnControlASO;
        FragmentNews fragmentNews = new FragmentNews();

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_home, container, false);
            btnNews = view.FindViewById<Button>(Resource.Id.buttonNews);
            btnAccount = view.FindViewById<Button>(Resource.Id.buttonAccount);
            btnControlASO = view.FindViewById<Button>(Resource.Id.buttonControlASO);
            //btnNews.Click += BtnNews_Click;
            btnNews.SetOnClickListener(this);
            btnAccount.SetOnClickListener(this);
            btnControlASO.SetOnClickListener(this);
            return view;
        }

        public void OnClick(View view)
        {

            switch (view.Id)
            {

                case Resource.Id.buttonNews:

                    Intent intentToActivityNews = new Intent(Context, typeof(ActivityNews));
                    this.StartActivity(intentToActivityNews);
                    break;

                case Resource.Id.buttonAccount:

                    this.StartActivity(new Intent(Context, typeof(ActivityAccount)));
                    break;

                case Resource.Id.buttonControlASO:

                    this.StartActivity(new Intent(Context, typeof(ActivityControlASO)));
                    break;

                case Resource.Id.buttonExit:

                    MainActivity mainActivity = new MainActivity();
                    mainActivity.FinishProgram();
                    break;


            };
        }


    }
}