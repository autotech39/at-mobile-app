﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace ASOmobile
{
    public class FragmentSensors : Android.Support.V4.App.Fragment
    {
        public static TextView txtTime;
        public static TextView txtSmoke;
        public static TextView txtBatASO;
        public static TextView txtBatAuto;
        public static TextView txtCurTemp;
        public static TextView txtFireTemp;
        public static TextView txtChangeTemp;
        public static TextView txtSpeed;
        public static TextView txtGPSLat;
        public static TextView txtGPSLong;
        public static TextView txtAccX;
        public static TextView txtAccY;
        public static TextView txtAccZ;

        public static TextView txtGyrX;
        public static TextView txtGyrY;
        public static TextView txtGyrZ;
        public static TextView txtMagX;
        public static TextView txtMagY;
        public static TextView txtMagZ;

        public static Handler handler;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment_sensors, container, false);
            txtTime = view.FindViewById<TextView>(Resource.Id.txtTime);
            txtSmoke = view.FindViewById<TextView>(Resource.Id.txtSmoke);
            txtBatASO = view.FindViewById<TextView>(Resource.Id.txtBatASO);
            txtBatAuto = view.FindViewById<TextView>(Resource.Id.txtBatAuto);
            txtCurTemp = view.FindViewById<TextView>(Resource.Id.txtCurTemp);
            txtFireTemp = view.FindViewById<TextView>(Resource.Id.txtFireTemp);
            txtChangeTemp = view.FindViewById<TextView>(Resource.Id.txtChangeTemp);
            txtSpeed = view.FindViewById<TextView>(Resource.Id.txtSpeed);
            txtGPSLat = view.FindViewById<TextView>(Resource.Id.txtGPSLat);
            txtGPSLong = view.FindViewById<TextView>(Resource.Id.txtGPSLong);
            txtAccX = view.FindViewById<TextView>(Resource.Id.txtAccX);
            txtAccY = view.FindViewById<TextView>(Resource.Id.txtAccY);
            txtAccZ = view.FindViewById<TextView>(Resource.Id.txtAccZ);
            txtGyrX = view.FindViewById<TextView>(Resource.Id.txtGyrX);
            txtGyrY = view.FindViewById<TextView>(Resource.Id.txtGyrY);
            txtGyrZ = view.FindViewById<TextView>(Resource.Id.txtGyrZ);
            txtMagX = view.FindViewById<TextView>(Resource.Id.txtMagX);
            txtMagY = view.FindViewById<TextView>(Resource.Id.txtMagY);
            txtMagZ = view.FindViewById<TextView>(Resource.Id.txtMagZ);

            handler = new Handler();

            FragmentSensors.txtTime.Text = MainActivity.txtTime;
            FragmentSensors.txtSmoke.Text = MainActivity.txtSmoke;
            FragmentSensors.txtBatASO.Text = MainActivity.txtBatASO;
            FragmentSensors.txtBatAuto.Text = MainActivity.txtBatAuto;
            FragmentSensors.txtCurTemp.Text = MainActivity.txtCurTemp;
            FragmentSensors.txtFireTemp.Text = MainActivity.txtFireTemp;
            FragmentSensors.txtChangeTemp.Text = MainActivity.txtChangeTemp;
            FragmentSensors.txtSpeed.Text = MainActivity.txtSpeed;
            FragmentSensors.txtGPSLat.Text = MainActivity.txtGPSLat;
            FragmentSensors.txtGPSLong.Text = MainActivity.txtGPSLong;
            FragmentSensors.txtAccX.Text = MainActivity.txtAccX;
            FragmentSensors.txtAccY.Text = MainActivity.txtAccY;
            FragmentSensors.txtAccZ.Text = MainActivity.txtAccZ;
            FragmentSensors.txtGyrX.Text = MainActivity.txtGyrX;
            FragmentSensors.txtGyrY.Text = MainActivity.txtGyrY;
            FragmentSensors.txtGyrZ.Text = MainActivity.txtGyrZ;
            FragmentSensors.txtMagX.Text = MainActivity.txtMagX;
            FragmentSensors.txtMagY.Text = MainActivity.txtMagY;
            FragmentSensors.txtMagZ.Text = MainActivity.txtMagZ;


            return view;
        }
    }
}